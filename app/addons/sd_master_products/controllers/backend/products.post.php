<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'update') {
        if (!fn_sd_master_products_is_admin() && fn_sd_master_products_is_slave($_REQUEST['product_id']) && 
            empty($_REQUEST['product_data']['product'])) {
            fn_companies_filter_company_product_categories($_REQUEST, $_REQUEST['product_data']);
            if (empty($_REQUEST['product_data']['category_ids'])) {
                fn_set_notification('E', __('error'), __('category_is_empty'));

                return array(CONTROLLER_STATUS_REDIRECT, !empty($_REQUEST['product_id']) ? "products.update?product_id=" . $_REQUEST['product_id'] : "products.add");
            } else {
                $_REQUEST['product_data']['category_ids'] = explode(',', $_REQUEST['product_data']['category_ids']);
            }

            $product_id = fn_update_product($_REQUEST['product_data'], $_REQUEST['product_id'], DESCR_SL);
        }

        if (!empty($_REQUEST['product_data']['is_master_product']) && 
            $_REQUEST['product_data']['is_master_product'] == 'Y') {
       
            db_query("UPDATE ?:products SET company_id = ?i WHERE product_id = ?i",
                99999, $_REQUEST['product_id']);

            $product_ids = db_get_fields("SELECT product_id FROM ?:products " .
                "WHERE cloned_from_product_id = ?i AND is_slave = ?s", 
                $_REQUEST['product_id'], 'Y');

            if (!empty($product_ids)) {
                if (!empty($_REQUEST['product_data']['is_master_product'])) {
                    $_REQUEST['product_data']['is_master_product'] = 'N';
                }
                if (!empty($_REQUEST['product_data']['company_id'])) {
                    unset($_REQUEST['product_data']['company_id']);
                }
                if (!empty($_REQUEST['product_data']['price'])) {
                    unset($_REQUEST['product_data']['price']);
                }
                if (!empty($_REQUEST['product_data']['prices'])) {
                    unset($_REQUEST['product_data']['prices']);
                }
                if (!empty($_REQUEST['product_data']['list_price'])) {
                    unset($_REQUEST['product_data']['list_price']);
                }
                if (!empty($_REQUEST['product_data']['amount'])) {
                    unset($_REQUEST['product_data']['amount']);
                }
                if (!empty($_REQUEST['product_data']['availability'])) {
                    unset($_REQUEST['product_data']['availability']);
                }
                if (!empty($_REQUEST['product_data']['qty_step'])) {
                    unset($_REQUEST['product_data']['qty_step']);
                }
                if (!empty($_REQUEST['product_data']['min_qty'])) {
                    unset($_REQUEST['product_data']['min_qty']);
                }
                if (!empty($_REQUEST['product_data']['max_qty'])) {
                    unset($_REQUEST['product_data']['max_qty']);
                }
                if (!empty($_REQUEST['product_data']['meta_description'])) {
                    unset($_REQUEST['product_data']['meta_description']);
                }
                if (!empty($_REQUEST['product_data']['meta_keywords'])) {
                   unset($_REQUEST['product_data']['meta_keywords']);
                }
                if (!empty($_REQUEST['product_data']['out_of_stock_actions'])) {
                    unset($_REQUEST['product_data']['out_of_stock_actions']);
                }
                unset($_REQUEST['product_data']['product_code']);
                foreach ($product_ids as $key => $pid) {
                    fn_update_product($_REQUEST['product_data'], $pid, DESCR_SL);
                    fn_clean_image_pairs($pid, 'product');
                    fn_clone_image_pairs($pid, $_REQUEST['product_id'], 'product');
                }        
            }
        }
    }
    return array(CONTROLLER_STATUS_OK);
}


if ($mode == 'manage') {

    $selected_fields = Registry::get('view')->getTemplateVars('selected_fields');
    if (fn_sd_master_products_is_admin()) {
        $selected_fields[] = array('name' => '[data][is_master_product]', 'text' => __('is_master_product'));        
    }
    $selected_fields[] = array('name' => '[data][availability]', 'text' => __('availability'));

    Registry::get('view')->assign('selected_fields', $selected_fields);

} elseif ($mode == 'm_update') {

    if (!empty($_SESSION['selected_fields'])) {
        $selected_fields = $_SESSION['selected_fields'];    
    }

    $field_groups = Registry::get('view')->getTemplateVars('field_groups');
    $filled_groups = Registry::get('view')->getTemplateVars('filled_groups');
    $field_names = Registry::get('view')->getTemplateVars('field_names');

    if (fn_sd_master_products_is_admin()) {
        if (!empty($selected_fields['data']['is_master_product'])) {
            $field_groups['C']['is_master_product'] = 'products_data';
            $filled_groups['C']['is_master_product'] = __('is_master_product');
            unset($field_names['is_master_product']);
        }
    }

    if (!empty($selected_fields['data']['availability'])) {
        $field_groups['S']['availability'] = array(
                'name' => 'products_data',
                'variants' => array (
                    'A' => 'always_available',
                    'M' => 'most_often',
                    'S' => 'sometimes',
                    'O' => 'against_order',
                    ),
                );

        $filled_groups['S']['availability'] = __('availability');
        unset($field_names['availability']);
    }

    Registry::get('view')->assign('field_groups', $field_groups);
    Registry::get('view')->assign('filled_groups', $filled_groups);
    Registry::get('view')->assign('field_names', $field_names);

} elseif ($mode == 'update') {

    if (fn_sd_master_products_is_slave($_REQUEST['product_id']) && !fn_sd_master_products_is_admin()) {
        $tabs = Registry::get('navigation.tabs'); 

        $tabs = array (
            'detailed' => array (
                'title' => __('general'),
                'js' => true
            ),
        );

        $tabs['addons'] = array (
            'title' => __('addons'),
            'js' => true
        );

        Registry::set('navigation.tabs', $tabs);
    }

}
