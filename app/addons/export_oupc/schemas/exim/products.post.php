<?php

use Tygh\Registry;

include_once(Registry::get('config.dir.addons') . 'export_oupc/schemas/exim/products.functions.php');

if ($_SESSION['auth']['user_type'] == 'A') { 
    
    $schema['export_fields']['OUPC'] = array('db_field' => 'omart_unique_product_code', 'required' => true);
}

$schema['export_fields']['Slave product'] = array(
    
    'db_field' => 'is_slave',
    'process_put' => array ('fn_exim_export_oupc_set_master_id', '#key', '#this'),
);

$schema['export_fields']['Ordinary product'] = array(
    
    'db_field' => 'is_slave',
    'process_get' => array ('fn_exim_export_oupc_get_ordinary_field', '#key', '#this'),
    'export_only' => true,
);

return $schema;