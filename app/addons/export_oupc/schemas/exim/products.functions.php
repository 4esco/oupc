<?php

function fn_exim_export_oupc_get_ordinary_field($product_id, $field_data)
{
    $is_master = db_get_field("SELECT is_master_product FROM ?:products WHERE product_id = ?i", $product_id);
    
    if ($is_master == "N" && $field_data == "N") {
        
        return("Y");
    }
    
    else {
        
        return("N");
    }
}

function fn_exim_export_oupc_set_master_id($product_id, $field_data)
{
    
    if ($field_data == 'Y') {
        
        $company_id = db_get_field("SELECT company_id FROM ?:products WHERE product_id = ?i", $product_id);

        $oupc = db_get_field("SELECT omart_unique_product_code FROM ?:products WHERE product_id = ?i", $product_id);
        
        $master_id = db_get_field("SELECT product_id FROM ?:products WHERE omart_unique_product_code = ?s AND is_master_product = ?s", $oupc, 'Y');
        
        $clone_locked = unserialize(db_get_field("SELECT clone_locked FROM ?:products WHERE product_id = ?i", $master_id));
        
        if (!empty($clone_locked)) {
        
            array_push($clone_locked, (int)$company_id);
        }
        
        else {
            
            $clone_locked = array();
            array_push($clone_locked, (int)$company_id);
        }
        
        db_query('UPDATE ?:products SET cloned_from_product_id = ?i WHERE product_id = ?i', $master_id, $product_id);
        
        db_query('UPDATE ?:products SET clone_locked = ?s WHERE product_id IN (?i, ?i)', serialize($clone_locked), $master_id, $product_id);
    }
    
    return true;
}